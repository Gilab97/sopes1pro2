package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	pb "./ecommerce"

	"google.golang.org/grpc"
	"github.com/gorilla/mux"
)

func getmessaje() string {

	conn, err := grpc.Dial("pythongrpc:9000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	c := pb.NewDiscountClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	r, err := c.Call(ctx, &pb.Chat{Body: "holas"})
	if err == nil {
		fmt.Println(r)
		log.Println(r.GetBody())
	} else {
		log.Println("Failed to apply discount.", err)
	}

	return `{ "hola": "nice"}`
}

func handlemensajes(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	productsWithDiscountApplied := getmessaje()
	json.NewEncoder(w).Encode(productsWithDiscountApplied)
}

type Article struct {
	Name      string `json:"name"`
	Location   string `json:"location"`
	Age   string `json:"age"`
	Infectedtype   string `json:"infectedtype"`
	State   string `json:"state"`
}

func crearmensaje(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	w.Header().Set("Content-Type", "application/json")

	//println("entra")
	reqBody, _ := ioutil.ReadAll(r.Body)

	//var article Article
	//json.Unmarshal(reqBody, &article)

	//fmt.Println(article)
	//println(article.Nota)
	//fmt.Println(string(reqBody))

	productsWithDiscountApplied := sendmessajepython( string(reqBody))
	json.NewEncoder(w).Encode(productsWithDiscountApplied)
	//json.NewEncoder(w).Encode(article)
}

func sendmessajepython(as string) string {

	conn, err := grpc.Dial("pythongrpc:9000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	c := pb.NewDiscountClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	r, err := c.Call(ctx, &pb.Chat{Body: as})
	if err == nil {
		fmt.Println(r)
		log.Println(r.GetBody())
		return r.GetBody()
	} else {
		log.Println("Failed to apply discount.", err)
	}

	return `{ "hola": "nice"}`
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}

func main() {
	port := "11080"

	myRouter := mux.NewRouter().StrictSlash(true)

	myRouter.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "It is working.")
	})

	myRouter.HandleFunc("/hola", handlemensajes)
	myRouter.HandleFunc("/hola2", crearmensaje).Methods("POST")

	fmt.Println("Server running on", port)
	http.ListenAndServe(":"+port, myRouter)
}
